import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MonederoComponent } from './monedero/monedero.component';
import { GameComponent } from './game/game.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'monedero', component: MonederoComponent },
	{ path: 'game', component: GameComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
